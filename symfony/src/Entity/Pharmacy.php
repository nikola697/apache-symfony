<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Pharmacy
 *
 * @ORM\Table(name="pharmacy")
 * @ORM\Entity
 * @ApiResource(
 *      itemOperations={"get"},
 *      collectionOperations={"get"}
 * )
 */
class Pharmacy
{
    /**
     * @var string
     *
     * @ORM\Column(name="PharmacyNumber", type="string", length=50, nullable=false)
     */
    private $pharmacynumber = '';

    /**
     * @var int
     *
     * @ORM\Column(name="PharmacyID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pharmacyid;

    /**
     * @var string
     *
     * @ORM\Column(name="PharmacyName", type="string", length=200, nullable=false)
     */
    private $pharmacyname = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PharmacyStreet", type="string", length=200, nullable=false)
     */
    private $pharmacystreet = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PharmacyPostCode", type="string", length=10, nullable=false)
     */
    private $pharmacypostcode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PharmacyCity", type="string", length=100, nullable=false)
     */
    private $pharmacycity = '';

    /**
     * @var string
     *
     * @ORM\Column(name="PharmacyRegion", type="string", length=2, nullable=false)
     */
    private $pharmacyregion = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="PharmacyIDFNumber", type="integer", nullable=true)
     */
    private $pharmacyidfnumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PharmacyPhoneNumber", type="string", length=100, nullable=true)
     */
    private $pharmacyphonenumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PharmacyTelefaxNumber", type="string", length=100, nullable=true)
     */
    private $pharmacytelefaxnumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PharmacyMailAccount", type="string", length=150, nullable=true)
     */
    private $pharmacymailaccount;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="PharmacyImportDate", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $pharmacyimportdate = 'CURRENT_TIMESTAMP';

    public function getPharmacynumber(): ?string
    {
        return $this->pharmacynumber;
    }

    public function setPharmacynumber(string $pharmacynumber): self
    {
        $this->pharmacynumber = $pharmacynumber;

        return $this;
    }

    public function getPharmacyid(): ?int
    {
        return $this->pharmacyid;
    }

    public function getPharmacyname(): ?string
    {
        return $this->pharmacyname;
    }

    public function setPharmacyname(string $pharmacyname): self
    {
        $this->pharmacyname = $pharmacyname;

        return $this;
    }

    public function getPharmacystreet(): ?string
    {
        return $this->pharmacystreet;
    }

    public function setPharmacystreet(string $pharmacystreet): self
    {
        $this->pharmacystreet = $pharmacystreet;

        return $this;
    }

    public function getPharmacypostcode(): ?string
    {
        return $this->pharmacypostcode;
    }

    public function setPharmacypostcode(string $pharmacypostcode): self
    {
        $this->pharmacypostcode = $pharmacypostcode;

        return $this;
    }

    public function getPharmacycity(): ?string
    {
        return $this->pharmacycity;
    }

    public function setPharmacycity(string $pharmacycity): self
    {
        $this->pharmacycity = $pharmacycity;

        return $this;
    }

    public function getPharmacyregion(): ?string
    {
        return $this->pharmacyregion;
    }

    public function setPharmacyregion(string $pharmacyregion): self
    {
        $this->pharmacyregion = $pharmacyregion;

        return $this;
    }

    public function getPharmacyidfnumber(): ?int
    {
        return $this->pharmacyidfnumber;
    }

    public function setPharmacyidfnumber(?int $pharmacyidfnumber): self
    {
        $this->pharmacyidfnumber = $pharmacyidfnumber;

        return $this;
    }

    public function getPharmacyphonenumber(): ?string
    {
        return $this->pharmacyphonenumber;
    }

    public function setPharmacyphonenumber(?string $pharmacyphonenumber): self
    {
        $this->pharmacyphonenumber = $pharmacyphonenumber;

        return $this;
    }

    public function getPharmacytelefaxnumber(): ?string
    {
        return $this->pharmacytelefaxnumber;
    }

    public function setPharmacytelefaxnumber(?string $pharmacytelefaxnumber): self
    {
        $this->pharmacytelefaxnumber = $pharmacytelefaxnumber;

        return $this;
    }

    public function getPharmacymailaccount(): ?string
    {
        return $this->pharmacymailaccount;
    }

    public function setPharmacymailaccount(?string $pharmacymailaccount): self
    {
        $this->pharmacymailaccount = $pharmacymailaccount;

        return $this;
    }

    public function getPharmacyimportdate(): ?\DateTimeInterface
    {
        return $this->pharmacyimportdate;
    }

    public function setPharmacyimportdate(?\DateTimeInterface $pharmacyimportdate): self
    {
        $this->pharmacyimportdate = $pharmacyimportdate;

        return $this;
    }


}
