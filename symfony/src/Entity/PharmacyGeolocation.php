<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PharmacyGeolocation
 *
 * @ORM\Table(name="pharmacy_geolocation")
 * @ORM\Entity
 */
class PharmacyGeolocation
{
    /**
     * @var int
     *
     * @ORM\Column(name="PharmacyID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pharmacyid = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="lat", type="decimal", precision=16, scale=12, nullable=true, options={"default"="0.000000000000"})
     */
    private $lat = '0.000000000000';

    /**
     * @var string|null
     *
     * @ORM\Column(name="long", type="decimal", precision=16, scale=12, nullable=true, options={"default"="0.000000000000"})
     */
    private $long = '0.000000000000';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ImportDate", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $importdate = 'CURRENT_TIMESTAMP';

    public function getPharmacyid(): ?int
    {
        return $this->pharmacyid;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function setLat($lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLong()
    {
        return $this->long;
    }

    public function setLong($long): self
    {
        $this->long = $long;

        return $this;
    }

    public function getImportdate(): ?\DateTimeInterface
    {
        return $this->importdate;
    }

    public function setImportdate(?\DateTimeInterface $importdate): self
    {
        $this->importdate = $importdate;

        return $this;
    }


}
