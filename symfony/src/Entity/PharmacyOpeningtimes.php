<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PharmacyOpeningtimes
 *
 * @ORM\Table(name="pharmacy_openingtimes")
 * @ORM\Entity
 */
class PharmacyOpeningtimes
{
    /**
     * @var int
     *
     * @ORM\Column(name="PharmacyID", type="integer", nullable=false)
     */
    private $pharmacyid;

    /**
     * @var int
     *
     * @ORM\Column(name="OpeningTimeID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $openingtimeid;

    /**
     * @var int
     *
     * @ORM\Column(name="WeekDayID", type="integer", nullable=false)
     */
    private $weekdayid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="OpeningStartMorning", type="string", length=45, nullable=true)
     */
    private $openingstartmorning;

    /**
     * @var string|null
     *
     * @ORM\Column(name="OpeningEndMorning", type="string", length=45, nullable=true)
     */
    private $openingendmorning;

    /**
     * @var string|null
     *
     * @ORM\Column(name="OpeningStartAfternoon", type="string", length=45, nullable=true)
     */
    private $openingstartafternoon;

    /**
     * @var string|null
     *
     * @ORM\Column(name="OpeningEndAfternoon", type="string", length=45, nullable=true)
     */
    private $openingendafternoon;

    /**
     * @var int
     *
     * @ORM\Column(name="StatusID", type="integer", nullable=false)
     */
    private $statusid;

    public function getPharmacyid(): ?int
    {
        return $this->pharmacyid;
    }

    public function setPharmacyid(int $pharmacyid): self
    {
        $this->pharmacyid = $pharmacyid;

        return $this;
    }

    public function getOpeningtimeid(): ?int
    {
        return $this->openingtimeid;
    }

    public function getWeekdayid(): ?int
    {
        return $this->weekdayid;
    }

    public function setWeekdayid(int $weekdayid): self
    {
        $this->weekdayid = $weekdayid;

        return $this;
    }

    public function getOpeningstartmorning(): ?string
    {
        return $this->openingstartmorning;
    }

    public function setOpeningstartmorning(?string $openingstartmorning): self
    {
        $this->openingstartmorning = $openingstartmorning;

        return $this;
    }

    public function getOpeningendmorning(): ?string
    {
        return $this->openingendmorning;
    }

    public function setOpeningendmorning(?string $openingendmorning): self
    {
        $this->openingendmorning = $openingendmorning;

        return $this;
    }

    public function getOpeningstartafternoon(): ?string
    {
        return $this->openingstartafternoon;
    }

    public function setOpeningstartafternoon(?string $openingstartafternoon): self
    {
        $this->openingstartafternoon = $openingstartafternoon;

        return $this;
    }

    public function getOpeningendafternoon(): ?string
    {
        return $this->openingendafternoon;
    }

    public function setOpeningendafternoon(?string $openingendafternoon): self
    {
        $this->openingendafternoon = $openingendafternoon;

        return $this;
    }

    public function getStatusid(): ?int
    {
        return $this->statusid;
    }

    public function setStatusid(int $statusid): self
    {
        $this->statusid = $statusid;

        return $this;
    }


}
