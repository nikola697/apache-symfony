<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PharmacyOpeningtimesStatus
 *
 * @ORM\Table(name="pharmacy_openingtimes_status")
 * @ORM\Entity
 */
class PharmacyOpeningtimesStatus
{
    /**
     * @var int
     *
     * @ORM\Column(name="StatusID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $statusid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="StatusName", type="string", length=45, nullable=true)
     */
    private $statusname;

    public function getStatusid(): ?int
    {
        return $this->statusid;
    }

    public function getStatusname(): ?string
    {
        return $this->statusname;
    }

    public function setStatusname(?string $statusname): self
    {
        $this->statusname = $statusname;

        return $this;
    }


}
