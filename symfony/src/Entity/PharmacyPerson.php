<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PharmacyPerson
 *
 * @ORM\Table(name="pharmacy_person")
 * @ORM\Entity
 */
class PharmacyPerson
{
    /**
     * @var int
     *
     * @ORM\Column(name="PharmacyPersonID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pharmacypersonid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PharmacyNumber", type="string", length=20, nullable=true)
     */
    private $pharmacynumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PharmacyPersonSurname", type="string", length=150, nullable=true)
     */
    private $pharmacypersonsurname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PharmacyPersonName", type="string", length=100, nullable=true)
     */
    private $pharmacypersonname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PharmacyPersonTitle", type="string", length=30, nullable=true)
     */
    private $pharmacypersontitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PharmacyPersonSalutation", type="string", length=20, nullable=true)
     */
    private $pharmacypersonsalutation;

    /**
     * @var int|null
     *
     * @ORM\Column(name="PharmacyPersonPosition", type="integer", nullable=true)
     */
    private $pharmacypersonposition;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="PharmacyPersonImportDate", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $pharmacypersonimportdate = 'CURRENT_TIMESTAMP';

    public function getPharmacypersonid(): ?int
    {
        return $this->pharmacypersonid;
    }

    public function getPharmacynumber(): ?string
    {
        return $this->pharmacynumber;
    }

    public function setPharmacynumber(?string $pharmacynumber): self
    {
        $this->pharmacynumber = $pharmacynumber;

        return $this;
    }

    public function getPharmacypersonsurname(): ?string
    {
        return $this->pharmacypersonsurname;
    }

    public function setPharmacypersonsurname(?string $pharmacypersonsurname): self
    {
        $this->pharmacypersonsurname = $pharmacypersonsurname;

        return $this;
    }

    public function getPharmacypersonname(): ?string
    {
        return $this->pharmacypersonname;
    }

    public function setPharmacypersonname(?string $pharmacypersonname): self
    {
        $this->pharmacypersonname = $pharmacypersonname;

        return $this;
    }

    public function getPharmacypersontitle(): ?string
    {
        return $this->pharmacypersontitle;
    }

    public function setPharmacypersontitle(?string $pharmacypersontitle): self
    {
        $this->pharmacypersontitle = $pharmacypersontitle;

        return $this;
    }

    public function getPharmacypersonsalutation(): ?string
    {
        return $this->pharmacypersonsalutation;
    }

    public function setPharmacypersonsalutation(?string $pharmacypersonsalutation): self
    {
        $this->pharmacypersonsalutation = $pharmacypersonsalutation;

        return $this;
    }

    public function getPharmacypersonposition(): ?int
    {
        return $this->pharmacypersonposition;
    }

    public function setPharmacypersonposition(?int $pharmacypersonposition): self
    {
        $this->pharmacypersonposition = $pharmacypersonposition;

        return $this;
    }

    public function getPharmacypersonimportdate(): ?\DateTimeInterface
    {
        return $this->pharmacypersonimportdate;
    }

    public function setPharmacypersonimportdate(?\DateTimeInterface $pharmacypersonimportdate): self
    {
        $this->pharmacypersonimportdate = $pharmacypersonimportdate;

        return $this;
    }


}
