<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * PharmacyServices
 *
 * @ORM\Table(name="pharmacy_services")
 * @ORM\Entity
 * @ApiResource(
 *      itemOperations={"get"},
 *      collectionOperations={"get"}
 * )
 */
class PharmacyServices
{
    /**
     * @var int
     *
     * @ORM\Column(name="PharmacyID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $pharmacyid;

    /**
     * @var int
     *
     * @ORM\Column(name="ServiceID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $serviceid;

    public function getPharmacyid(): ?int
    {
        return $this->pharmacyid;
    }

    public function getServiceid(): ?int
    {
        return $this->serviceid;
    }


}
