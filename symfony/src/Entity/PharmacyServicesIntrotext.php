<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PharmacyServicesIntrotext
 *
 * @ORM\Table(name="pharmacy_services_introtext")
 * @ORM\Entity
 */
class PharmacyServicesIntrotext
{
    /**
     * @var int
     *
     * @ORM\Column(name="PharmacyID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pharmacyid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PharmacyServiceIntroText", type="text", length=65535, nullable=true)
     */
    private $pharmacyserviceintrotext;

    public function getPharmacyid(): ?int
    {
        return $this->pharmacyid;
    }

    public function getPharmacyserviceintrotext(): ?string
    {
        return $this->pharmacyserviceintrotext;
    }

    public function setPharmacyserviceintrotext(?string $pharmacyserviceintrotext): self
    {
        $this->pharmacyserviceintrotext = $pharmacyserviceintrotext;

        return $this;
    }


}
