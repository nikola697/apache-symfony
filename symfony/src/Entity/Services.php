<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Services
 *
 * @ORM\Table(name="services")
 * @ORM\Entity
 * @ApiResource(
 *      itemOperations={"get"},
 *      collectionOperations={"get"}
 * )
 */
class Services
{
    /**
     * @var int
     *
     * @ORM\Column(name="ServiceID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $serviceid;

    /**
     * @var int
     *
     * @ORM\Column(name="ServiceParentID", type="integer", nullable=false)
     */
    private $serviceparentid = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ServiceName", type="string", length=255, nullable=true)
     */
    private $servicename;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ServiceTitle", type="string", length=255, nullable=true)
     */
    private $servicetitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ServiceDescriptionShort", type="string", length=150, nullable=true)
     */
    private $servicedescriptionshort;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ServiceDescriptionLong", type="string", length=255, nullable=true)
     */
    private $servicedescriptionlong;

    /**
     * @var int
     *
     * @ORM\Column(name="ServiceLegacyID", type="integer", nullable=false)
     */
    private $servicelegacyid = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="ServiceEmphasized", type="boolean", nullable=true)
     */
    private $serviceemphasized;

    public function getServiceid(): ?int
    {
        return $this->serviceid;
    }

    public function getServiceparentid(): ?int
    {
        return $this->serviceparentid;
    }

    public function setServiceparentid(int $serviceparentid): self
    {
        $this->serviceparentid = $serviceparentid;

        return $this;
    }

    public function getServicename(): ?string
    {
        return $this->servicename;
    }

    public function setServicename(?string $servicename): self
    {
        $this->servicename = $servicename;

        return $this;
    }

    public function getServicetitle(): ?string
    {
        return $this->servicetitle;
    }

    public function setServicetitle(?string $servicetitle): self
    {
        $this->servicetitle = $servicetitle;

        return $this;
    }

    public function getServicedescriptionshort(): ?string
    {
        return $this->servicedescriptionshort;
    }

    public function setServicedescriptionshort(?string $servicedescriptionshort): self
    {
        $this->servicedescriptionshort = $servicedescriptionshort;

        return $this;
    }

    public function getServicedescriptionlong(): ?string
    {
        return $this->servicedescriptionlong;
    }

    public function setServicedescriptionlong(?string $servicedescriptionlong): self
    {
        $this->servicedescriptionlong = $servicedescriptionlong;

        return $this;
    }

    public function getServicelegacyid(): ?int
    {
        return $this->servicelegacyid;
    }

    public function setServicelegacyid(int $servicelegacyid): self
    {
        $this->servicelegacyid = $servicelegacyid;

        return $this;
    }

    public function getServiceemphasized(): ?bool
    {
        return $this->serviceemphasized;
    }

    public function setServiceemphasized(?bool $serviceemphasized): self
    {
        $this->serviceemphasized = $serviceemphasized;

        return $this;
    }


}
