<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Weekdays
 *
 * @ORM\Table(name="weekdays")
 * @ORM\Entity
 */
class Weekdays
{
    /**
     * @var int
     *
     * @ORM\Column(name="WeekDayID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $weekdayid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="WeekDayNamelong", type="string", length=100, nullable=true)
     */
    private $weekdaynamelong;

    /**
     * @var string|null
     *
     * @ORM\Column(name="WeekDayNameshort", type="string", length=20, nullable=true)
     */
    private $weekdaynameshort;

    public function getWeekdayid(): ?int
    {
        return $this->weekdayid;
    }

    public function getWeekdaynamelong(): ?string
    {
        return $this->weekdaynamelong;
    }

    public function setWeekdaynamelong(?string $weekdaynamelong): self
    {
        $this->weekdaynamelong = $weekdaynamelong;

        return $this;
    }

    public function getWeekdaynameshort(): ?string
    {
        return $this->weekdaynameshort;
    }

    public function setWeekdaynameshort(?string $weekdaynameshort): self
    {
        $this->weekdaynameshort = $weekdaynameshort;

        return $this;
    }


}
